class Torrent
  def initialize(raw_hash)
    @raw_hash = raw_hash
    serialize.each do |k, v|
      instance_variable_set "@#{k}", v
      self.class.__send__(:attr_reader, k)
    end
  end

  def serialize
    serialized_queries = Hash[
      @raw_hash.map do |k, v|
        [
          k,
          mutate(FLAT_QUERIES[k][:kind], v)
        ]
      end
    ]

    serialized_queries.merge({
      tracker: URI(serialized_queries[:tracker_url]).host
    })
  end

  private

  def mutate(kase, data)
    case kase
      when :time
        Time.at(data).to_i
      when :ratio
        data.to_f / 1000
      when :int
        data.to_i
      when :bool
        !!data
      else
        data
    end
  end
end
